<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

echo "<h1>Output</h1>";

$sheep = new Animal("Shaun");

echo "Nama : $sheep->name  <br>";
echo "Legs :  $sheep->legs  <br>";
echo "Cold Bloodes :  $sheep->cold_blooded <br>"; 
echo "<br>";

$kodok = new Frog("Buduk");

echo "Nama : $kodok->name  <br>";
echo "Legs :  $kodok->legs  <br>";
echo "Cold Bloodes :  $kodok->cold_blooded <br>";
echo $kodok->jump(); 
echo "<br><br>";

$shungkong = new Ape("Kera Sakti");

echo "Nama : $shungkong->name  <br>";
echo "Legs :  $shungkong->legs  <br>";
echo "Cold Bloodes :  $shungkong->cold_blooded <br>"; 
echo $shungkong->yell();

?>